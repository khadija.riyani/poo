package dto;

public class Electrodomestico {
	
	//Atributos
	
	private double Precio_Base;
	private String color;
	private String consumo_energetico ="A,F";
	private double peso;
	
	
	public Electrodomestico (){
		this.Precio_Base = 100;
		this.color = "Blanco";
		this.consumo_energetico = "F";
		this.peso = 5;
	}

	Electrodomestico (double Precio_Base, String color, String consumo_energetico , double peso){
		this.Precio_Base = Precio_Base;
		this.color = color;
		this.consumo_energetico = consumo_energetico;
		this.peso = peso;
	}
	
	public void  EleccionColor() {
		String color []= {"blanco", "negro", "rojo", "azul", "gris"};
		
		for(int i = 0; i < color.length;i++) {
			
			System.out.println("EL color es " + color[i]);		
		}
		
		
	}

	/**
	 * @return the precio_Base
	 */
	public double getPrecio_Base() {
		return Precio_Base;
	}

	/**
	 * @param precio_Base the precio_Base to set
	 */
	public void setPrecio_Base(double precio_Base) {
		Precio_Base = precio_Base;
	}

	/**
	 * @return the color
	 */
	public String getColor() {
		return color;
	}

	/**
	 * @param color the color to set
	 */
	public void setColor(String color) {
		this.color = color;
	}

	/**
	 * @return the consumo_energetico
	 */
	public String getConsumo_energetico() {
		return consumo_energetico;
	}

	/**
	 * @param consumo_energetico the consumo_energetico to set
	 */
	public void setConsumo_energetico(String consumo_energetico) {
		this.consumo_energetico = consumo_energetico;
	}

	/**
	 * @return the peso
	 */
	public double getPeso() {
		return peso;
	}

	/**
	 * @param peso the peso to set
	 */
	public void setPeso(double peso) {
		this.peso = peso;
	}
	
	
}
