package dto;

public class Contraseña {

	//Atributos
	
    private String contraseña;
	private int longitud;
	
	//Constructores
	public Contraseña(){
		
		this.contraseña = " ";
		this.longitud = 8;
	}
	
	public Contraseña(String contraseña,int longitud ) {
		
		this.contraseña = contraseña;
		this.longitud = longitud;
		
	}


	public int DevuelveLongitud () {
		return longitud;
	}
	
	
	public int ObtenContraseña() {
		
		
		int contra =0;
		
		for ( int i = 0; i < longitud; i ++ ) {
			contra = ((int)Math.random() * 3 + 1);
			
		}
		
		return contra;
		
		
		
	}

	/**
	 * @return the contraseña
	 */
	public String getContraseña() {
		return contraseña;
	}

	/**
	 * @param contraseña the contraseña to set
	 */
	public void setContraseña(String contraseña) {
		this.contraseña = contraseña;
	}

	/**
	 * @return the longitud
	 */
	public int getLongitud() {
		return longitud;
	}

	/**
	 * @param longitud the longitud to set
	 */
	public void setLongitud(int longitud) {
		this.longitud = longitud;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
